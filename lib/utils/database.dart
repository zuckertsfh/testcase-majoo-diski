import 'package:majootestcase/model/model.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

class LocalDB {
  Future<String> pathDB() async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, 'user.db');
    return path;
  }

  Future<void> initial() async {
    try {
      String _pathDB = await pathDB();

      Database database = await openDatabase(_pathDB, version: 1,
          onCreate: (Database db, int version) async {
        await db.execute(
            'CREATE TABLE User (id INTEGER PRIMARY KEY, email TEXT, username TEXT,password TEXT)');
      });

      List<Map> users = await database.rawQuery("SELECT * from User");
      if (users.isEmpty) {
        await database.rawInsert(
            "INSERT INTO User(email, username,password) VALUES(?, ?, ?)",
            ['majoo@gmail.com', 'majoo', '123456']);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<void> addUser(
      {required String email,
      required String password,
      required String username}) async {
    try {
      Database database = await openDatabase(await pathDB(), version: 1);

      List<Map> user =
          await database.query("User", where: 'email = ?', whereArgs: [email]);
      if (user.isNotEmpty) throw "Akun ini sudah terdaftar";

      user = await database
          .query("User", where: 'username = ?', whereArgs: [username]);
      if (user.isNotEmpty)
        throw "Username telah dipakai, coba gunakan yang lain";

      await database.rawInsert(
          "INSERT INTO User(email, username, password) VALUES(?, ?, ?)",
          [email, username, password]);
    } catch (e) {
      throw e;
    }
  }

  Future<User?> getUser(
      {required String email, required String password}) async {
    try {
      Database database = await openDatabase(await pathDB(), version: 1);

      List<Map> user = await database.query("User",
          where: 'email = ? AND password = ?',
          whereArgs: [email, password],
          limit: 1);
      if (user.isEmpty) return null;
      return User.fromJson(Map<String, dynamic>.from(user.first));
    } catch (e) {
      throw e;
    }
  }
}

class Preference {
  Future<SharedPreferences> open() async =>
      await SharedPreferences.getInstance();

  Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await open();
    return prefs.getBool("is_logged_in") ?? false;
  }

  Future<void> setLoggedIn() async {
    await open()
      ..setBool("is_logged_in", true);
  }

  Future<void> setUser(String user) async {
    await open()
      ..setString("user_value", user);
  }

  Future<void> removeAll() async {
    await open()
      ..remove('is_logged_in');
    await open()
      ..remove('user_value');
  }
}

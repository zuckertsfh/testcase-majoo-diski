import 'package:connectivity_plus/connectivity_plus.dart';

Future<bool> hasConnection() async {
  ConnectivityResult connectivityResult =
      await Connectivity().checkConnectivity();
  return connectivityResult != ConnectivityResult.none &&
      connectivityResult != ConnectivityResult.bluetooth;
}

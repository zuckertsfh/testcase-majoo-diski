import 'package:flutter/material.dart';
class Api {
  static const BASE_URL = "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class CustomTheme {
  static const Color kWhiteBgColor = Color(0xFFF6F9FF);
  static const Color kBlackColor = Color(0xFF303030);
  static const Color kWhiteColor = Color(0xFFFFFFFF);
}
class Regex {
  final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
}

import 'package:flutter/foundation.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:majootestcase/routes/routes.dart';
import 'package:majootestcase/ui/page/page.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(ModularApp(module: AppRoutes(), child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthBlocCubit(),
        ),
        BlocProvider(
          create: (context) => HomeBlocCubit(),
        )
      ],
      child: GlobalLoaderOverlay(
        useDefaultLoading: false,
        overlayOpacity: 0.8,
        overlayColor: Colors.black87,
        overlayWidget: Center(
          child: CircularProgressIndicator(),
        ),
        child: MaterialApp(
          title: 'Testcase Majoo',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          initialRoute: '/',
        ).modular(),
      ),
    );
  }
}
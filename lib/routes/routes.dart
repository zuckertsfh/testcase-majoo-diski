import 'package:flutter_modular/flutter_modular.dart';
import 'package:majootestcase/ui/page/page.dart';

class AppRoutes extends Module {
  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (_, __) => SplashPage()),
        ChildRoute('/login', child: (_, __) => LoginPage()),
        ChildRoute('/register', child: (_, __) => RegisterPage()),
        ChildRoute('/home', child: (_, __) => HomePage()),
        ChildRoute('/movie',
            child: (context, args) => DetailMoviePage(args.data)),
        ChildRoute('/errorConnection', child: (_, __) => ConnectionErrorPage()),
      ];
}

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/model/model.dart';
import 'package:majootestcase/services/dio_config_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchData() async {
    try {
      emit(HomeBlocLoadingState());
      MovieResponse movieResponse = await ApiServices().getMovieList();
      emit(HomeBlocLoadedState(movieResponse.data));
    } catch (e) {
      emit(HomeBlocErrorState(e.toString()));
    }
  }
}

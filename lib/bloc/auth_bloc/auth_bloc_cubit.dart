import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/model/model.dart';
import 'package:majootestcase/utils/database.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    if (await Preference().isLoggedIn()) {
      emit(AuthBlocLoggedInState());
    } else {
      emit(AuthBlocLoginState());
    }
  }

  void loginUser(User user) async {
    try {
      emit(AuthBlocLoadingState());

      User? _user =
          await LocalDB().getUser(email: user.email, password: user.password);
      if (_user == null)
        emit(AuthBlocErrorState("Login gagal, periksa kembali inputan anda"));

      await Preference().setLoggedIn();
      await Preference().setUser(_user!.toJson().toString());

      emit(AuthBlocLoggedInState());
    } catch (e) {
      emit(AuthBlocErrorState(e));
    }
  }

  void registerUser(User user) async {
    try {
      emit(AuthBlocLoadingState());
      await LocalDB().addUser(
          email: user.email,
          username: user.userName ?? "",
          password: user.password);
      await Preference().setLoggedIn();
      await Preference().setUser(user.toJson().toString());
      emit(AuthBlocLoggedInState());
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }

  void logout() async {
    try {
      emit(AuthBlocLoadingState());
      await Preference().removeAll();
      emit(AuthBlocLoginState());
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }
}

part of '../widget.dart';

class ButtonPrimary extends StatelessWidget {
  final String title;
  final Function() callbackfunc;
  final double height;
  final double width;
  const ButtonPrimary(
      {Key? key,
      required this.title,
      required this.callbackfunc,
      this.height = 50,
      this.width = double.infinity})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          // primary: ,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          )
        ),
        onPressed: callbackfunc,
        child: Text(title),
      ),
    );
  }
}

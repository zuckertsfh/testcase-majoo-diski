part of 'widget.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              message,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14, color: textColor ?? Colors.black),
            ),
            retry != null
                ? TextButton(
                    onPressed: retry,
                    child: Text("Refresh"),
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}

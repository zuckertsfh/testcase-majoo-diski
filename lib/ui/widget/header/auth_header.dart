part of '../widget.dart';

class AuthHeader {
  static List<Widget> header(String title, String subtitle) {
    return [
      Text(
        "$title",
        style: TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.bold,
          // color: colorBlue,
        ),
      ),
      Text(
        "$subtitle",
        style: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.w400,
        ),
      ),
    ];
  }
}

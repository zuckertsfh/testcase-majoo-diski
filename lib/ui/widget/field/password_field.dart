part of '../widget.dart';

class CustomPasswordField extends StatefulWidget {
  final String? hint;
  final TextEditingController controller;
  final TextInputAction textInputAction;
  final bool hidePass;
  final String? label;
  const CustomPasswordField(
    this.controller, {
    Key? key,
    this.hint,
    this.hidePass = false,
    this.label,
    this.textInputAction = TextInputAction.next,
  }) : super(key: key);

  @override
  _CustomPasswordFieldState createState() => _CustomPasswordFieldState();
}

class _CustomPasswordFieldState extends State<CustomPasswordField> {
  bool hidePass = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: widget.controller,
        textInputAction: widget.textInputAction,
        keyboardType: TextInputType.visiblePassword,
        validator: (String? v) {
          if (v == null || v.isEmpty) {
            return "Form tidak boleh kosong";
          }
          return null;
        },
        obscureText: hidePass,
        decoration: InputDecoration(
          hintText: widget.hint,
          labelText: widget.label,
          suffixIcon: IconButton(
              onPressed: () {
                setState(() {
                  hidePass = !hidePass;
                });
              },
              icon: Icon(hidePass ? Icons.visibility_off : Icons.visibility)),
        ),
      ),
    );
  }
}

part of '../widget.dart';

class CustomTextField extends StatelessWidget {
  final String? hint;
  final TextEditingController controller;
  final TextInputType? textInputType;
  final TextInputAction textInputAction;
  final String? Function(String? v)? validator;
  final String? label;
  const CustomTextField(
    this.controller, {
    Key? key,
    this.hint,
    this.textInputType,
    this.validator,
    this.label,
    this.textInputAction = TextInputAction.next,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: controller,
        keyboardType: textInputType,
        textInputAction: textInputAction,
        validator: validator ??
            (String? v) {
              if (v == null || v.isEmpty) {
                return "Form tidak boleh kosong";
              }

              if (textInputType == TextInputType.emailAddress &&
                  !Regex().pattern.hasMatch(v)) {
                return 'Masukkan email yang valid';
              }

              return null;
            },
        decoration: InputDecoration(hintText: hint, labelText: label),
      ),
    );
  }
}

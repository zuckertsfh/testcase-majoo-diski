part of '../../page.dart';

class DetailMoviePage extends StatelessWidget {
  final DataMovie data;
  const DetailMoviePage(this.data, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool hasSeries = data.series.isNotEmpty;

    Widget series() => hasSeries
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  "Series",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                ),
              ),
              SizedBox(
                height: 200,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: data.series.length,
                  itemBuilder: (context, i) {
                    Series series = data.series[i];
                    return Container(
                      padding: EdgeInsets.only(
                        left: i == 0 ? 16 : 8,
                        right: i + 1 == data.series.length ? 16 : 8,
                      ),
                      width: 180,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 100,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                image: NetworkImage(
                                  series.i.imageUrl,
                                ),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            series.l,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 4,
                          ),
                        ],
                      ),
                    );
                  },
                ),
              )
            ],
          )
        : Container(
            margin: EdgeInsets.only(top: 100),
            alignment: Alignment.center,
            child: Text(
              "Tidak ada series",
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          );

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            "Detail Movies",
            style: TextStyle(color: CustomTheme.kBlackColor),
          ),
          leading: IconButton(
            onPressed: () {
              Modular.to.pop();
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: CustomTheme.kBlackColor,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // IMAGE
              Container(
                height: 250,
                child: Image.network(
                  data.i.imageUrl,
                  fit: BoxFit.contain,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              // TITLE
              Center(
                child: Text(
                  data.l,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: CustomTheme.kBlackColor,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                ),
              ),
              // YEAR
              Center(
                child: Text(
                  data.yr ?? data.year.toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: CustomTheme.kBlackColor),
                ),
              ),
              // LIST SERIES
              series(),
            ],
          ),
        ),
      ),
    );
  }
}

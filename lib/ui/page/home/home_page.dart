part of '../page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    _fetch();
  }

  // HANDLE FUNCTION
  Future<void> _fetch() async {
    if (await hasConnection()) context.read<HomeBlocCubit>().fetchData();
    else Modular.to.navigate('/errorConnection');
  }

  void _listener(BuildContext context, AuthBlocState state) {
    if (state is AuthBlocLoadingState) {
      context.loaderOverlay.show();
    }

    if (state is! AuthBlocLoadingState) {
      context.loaderOverlay.hide();
    }

    if (state is AuthBlocLoginState) {
      Modular.to.navigate('/login');
    }

    if (state is AuthBlocErrorState) {
      showSnackbar(context, state.error);
    }
  }

  @override
  Widget build(BuildContext context) {
    // HANDLE WIDGET
    Widget _header() => Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Movies",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
              ),
              BlocListener<AuthBlocCubit, AuthBlocState>(
                listener: _listener,
                child: IconButton(
                  onPressed: () {
                    context.read<AuthBlocCubit>().logout();
                  },
                  icon: Icon(Icons.logout_rounded),
                  tooltip: "Logout",
                ),
              ),
            ],
          ),
        );

    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            _header(),
            Expanded(
              child: BlocBuilder<HomeBlocCubit, HomeBlocState>(
                  builder: (context, state) {
                if (state is HomeBlocLoadedState) {
                  return RefreshIndicator(
                      onRefresh: _fetch, child: HomeListData(data: state.data));
                } else if (state is HomeBlocLoadingState) {
                  return LoadingIndicator();
                } else if (state is HomeBlocInitialState) {
                  return Scaffold();
                } else if (state is HomeBlocErrorState) {
                  return Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: ErrorScreen(
                      message: state.error,
                      retry: _fetch,
                    ),
                  );
                }

                return Center(
                    child:
                        Text(kDebugMode ? "state not implemented $state" : ""));
              }),
            ),
          ],
        ),
      ),
    );
  }
}

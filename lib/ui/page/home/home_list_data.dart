part of '../page.dart';

class HomeListData extends StatelessWidget {
  final List<DataMovie> data;

  const HomeListData({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomTheme.kWhiteBgColor,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(
              left: 16,
              right: 16,
              top: index == 0 ? 16 : 8,
              bottom: index == data.length - 1 ? 16 : 8,
            ),
            child: InkWell(
                onTap: () {
                  Modular.to.pushNamed('/movie', arguments: data[index]);
                },
                child: movieItemWidget(data[index])),
          );
        },
      ),
    );
  }

  Widget movieItemWidget(DataMovie data) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0))),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(25),
            child: Image.network(data.i.imageUrl),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text(data.l, textDirection: TextDirection.ltr),
          )
        ],
      ),
    );
  }
}

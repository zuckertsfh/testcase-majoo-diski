part of 'page.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    context.read<AuthBlocCubit>().fetchHistoryLogin();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      listener: (context, state) {
        if (state is AuthBlocLoginState){
          Modular.to.navigate('/login');
        }

        if (state is AuthBlocLoggedInState){
          Modular.to.navigate('/home');
        }
      },
      child: Scaffold(
        body: Container(),
      ),
    );
  }
}

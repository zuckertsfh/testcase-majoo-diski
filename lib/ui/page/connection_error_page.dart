part of 'page.dart';

class ConnectionErrorPage extends StatefulWidget {
  const ConnectionErrorPage({Key? key}) : super(key: key);

  @override
  _ConnectionErrorPageState createState() => _ConnectionErrorPageState();
}

class _ConnectionErrorPageState extends State<ConnectionErrorPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                "assets/images/connection_error.svg",
                height: 150,
              ),
              const SizedBox(
                height: 50,
              ),
              Text(
                "Pastikan kamu terhubung dengan internet",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16),
              ),
              TextButton(
                onPressed: () async {
                  if (await hasConnection())
                    Modular.to.navigate('/home');
                  else
                    showSnackbar(context, "Belum terhubung dengan internet");
                },
                child: Text("Refresh"),
              )
            ],
          ),
        ),
      ),
    );
  }
}

part of '../page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  late TextEditingController _emailController;
  late TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();
    LocalDB().initial();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }

  // HANDLE FUNCTION
  void handleLogin() async {
    final _email = _emailController.text.trim();
    final _password = _passwordController.text.trim();

    if (formKey.currentState!.validate()) {
      User user = User(
        email: _email,
        password: _password,
      );
      context.read<AuthBlocCubit>().loginUser(user);
    }
  }

  void _listener(BuildContext context, AuthBlocState state) {
    if (state is AuthBlocLoadingState) {
      context.loaderOverlay.show();
    }

    if (state is! AuthBlocLoadingState) {
      context.loaderOverlay.hide();
    }

    if (state is AuthBlocLoggedInState) {
      showSnackbar(context, "Login Berhasil");
      Modular.to.navigate('/home');
    }

    if (state is AuthBlocErrorState) {
      showSnackbar(context, state.error);
    }
  }

  @override
  Widget build(BuildContext context) {
    // HANDLE WIDGETS
    Widget _form() {
      return Form(
        key: formKey,
        child: Column(
          children: [
            CustomTextField(
              _emailController,
              hint: 'Masukkan alamat email',
              label: 'Email',
              textInputType: TextInputType.emailAddress,
            ),
            CustomPasswordField(
              _passwordController,
              hint: 'Masukkan password',
              label: 'Password',
              textInputAction: TextInputAction.done,
            )
          ],
        ),
      );
    }

    Widget _register() {
      return Center(
        child: TextButton(
          onPressed: () async {
            Modular.to.navigate('/register');
          },
          child: RichText(
            text: TextSpan(
                text: 'Belum punya akun? ',
                style: TextStyle(color: Colors.black45),
                children: [
                  TextSpan(
                    text: 'Daftar',
                  ),
                ]),
          ),
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        body: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: _listener,
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ...AuthHeader.header(
                      "Login", "Silahkan login terlebih dahulu"),
                  SizedBox(
                    height: 9,
                  ),
                  _form(),
                  SizedBox(
                    height: 50,
                  ),
                  ButtonPrimary(title: "Login", callbackfunc: handleLogin),
                  SizedBox(
                    height: 10,
                  ),
                  _register(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

part of '../page.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();

  late TextEditingController username;
  late TextEditingController email;
  late TextEditingController password;

  @override
  void initState() {
    super.initState();
    username = TextEditingController();
    email = TextEditingController();
    password = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    username.dispose();
    email.dispose();
    password.dispose();
  }

  // HANDLE FUNCTION
  void _register() {
    if (_formKey.currentState!.validate()) {
      User user = User(
        userName: username.text.trim(),
        email: email.text.trim(),
        password: password.text.trim(),
      );
      context.read<AuthBlocCubit>().registerUser(user);
    }
  }

  void _listener(BuildContext context, AuthBlocState state) {
    if (state is AuthBlocLoadingState) {
      context.loaderOverlay.show();
    }

    if (state is! AuthBlocLoadedState) {
      context.loaderOverlay.hide();
    }

    if (state is AuthBlocLoggedInState) {
      showSnackbar(context, "Register Berhasil");
      Modular.to.navigate('/home');
    }

    if (state is AuthBlocErrorState) {
      showSnackbar(context, state.error);
    }
  }

  @override
  Widget build(BuildContext context) {
    // HANDLE WIDGETS
    List<Widget> _form() {
      return [
        CustomTextField(
          username,
          hint: "Masukkan username",
          label: "Username",
        ),
        const SizedBox(
          height: 15,
        ),
        CustomTextField(
          email,
          hint: "Masukkan email",
          label: "Email",
          textInputType: TextInputType.emailAddress,
        ),
        const SizedBox(
          height: 15,
        ),
        CustomPasswordField(
          password,
          hint: "Masukkan password",
          label: "Password",
          textInputAction: TextInputAction.done,
        ),
      ];
    }

    return WillPopScope(
      onWillPop: () async {
        Modular.to.navigate('/login');
        return false;
      },
      child: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: _listener,
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                onPressed: () {
                  Modular.to.navigate('/login');
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: CustomTheme.kBlackColor,
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ...AuthHeader.header(
                          "Register", "Daftarkan diri anda segera"),
                      const SizedBox(
                        height: 30,
                      ),
                      ..._form(),
                      const SizedBox(
                        height: 35,
                      ),
                      ButtonPrimary(
                        title: "Register",
                        callbackfunc: _register,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

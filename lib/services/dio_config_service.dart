import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/error_helper.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:majootestcase/model/model.dart';

part 'api_service.dart';

mixin Services {
  late Dio dioInstance;
  createInstance() async {
    var options = BaseOptions(
        baseUrl: Api.BASE_URL,
        connectTimeout: 12000,
        receiveTimeout: 12000,
        headers: {
          "x-rapidapi-key":
              "8c690da628msh0cb544e56b00a17p1e750fjsn74834e33c5d0",
          "x-rapidapi-host": "imdb8.p.rapidapi.com"
        });
    dioInstance = new Dio(options);
    dioInstance.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
  }

  Future<Dio> dio() async {
    await createInstance();
    dioInstance.options.baseUrl = Api.BASE_URL;
    return dioInstance;
  }
}

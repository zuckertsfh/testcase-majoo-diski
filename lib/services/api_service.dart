part of 'dio_config_service.dart';
class ApiServices with Services{

  Future<MovieResponse> getMovieList() async {
    try {
      var _dio = await dio();
      Response<String> response  = await _dio.get("");
      MovieResponse movieResponse = MovieResponse.fromJson(jsonDecode(response.data!));
      return movieResponse;
    }on DioError catch(e) {
      throw ErrorHelper.extractApiError(e);
    }
  }
}